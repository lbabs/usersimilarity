package usersimilarity;

import Constructors.SelectionQuery;
import Constructors.Tweets;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import lucene.TweetSimilarity;

/**
 *
 * @author lebab
 */
public class ValueDistance {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ValueDistance.class);

    /**
     *
     * @param data A list that holds the data of the first user
     * @param data2 A list that holds the data of the second user
     * @param choise Choose polarity or date
     * @return A float number which is the score - the similarity of the two
     * users
     * @throws ParseException
     * @throws FileNotFoundException
     * @throws IOException
     * @throws org.apache.lucene.queryparser.classic.ParseException
     */
    public float compute(List<SelectionQuery> data, List<SelectionQuery> data2, String choise,Integer flagChoose) throws ParseException, FileNotFoundException, IOException, org.apache.lucene.queryparser.classic.ParseException {


        /**
         * Initialization of an array that holds the values in order to compute
         * the modified edit distance
         */
        float[][] dp = null;
        float p = 0.0f;
        String codeDatePol1 = "";
        String codeDatePol2 = "";
        dp = new float[data.size() + 1][data2.size() + 1];
        log.info("Inserting to ValueDistance()");
        int flag=0;

        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < dp[i].length; j++) {
                dp[i][j] = i == 0 ? j : j == 0 ? i : 0;
                if (i > 0 && j > 0) {
                    if (choise.equals("date") ) {
                        log.debug("Entering the choise date...");
                        codeDatePol1 = data.get(i - 1).getLabel();
                        codeDatePol2 = data2.get(j - 1).getLabel();
                    } else if (choise.equals("polarity") ) {
                        log.debug("Entering the choise polarity...");
                        codeDatePol1 = data.get(i - 1).getPolarity();
                        codeDatePol2 = data2.get(j - 1).getPolarity();
                    } 
                    p = content(data.get(i - 1).getTweet(), data.get(i - 1).getUsername(), data2.get(j - 1).getTweet(), data2.get(j - 1).getUsername());
                    dp[i][j] = Math.min(Math.min(dp[i][j - 1] + 1, dp[i - 1][j] + 1), (dp[i - 1][j - 1] + ((codeDatePol1.equals(codeDatePol2)) ? p : 1)));
                }
            }
        }
        log.debug("Return the similarity of the two users...");
        return (float) round(dp[data.size()][data2.size()],3);
    }

    /**
     *
     * @param tweet1 Tweet of the first user
     * @param un1 Username of the first user
     * @param tweet2 Tweet of the second user
     * @param un2 Username of the second user
     * @return The score between tweets of a user - float -
     * @throws ParseException
     * @throws FileNotFoundException
     * @throws IOException
     * @throws org.apache.lucene.queryparser.classic.ParseException
     */
    public float content(String tweet1, String un1, String tweet2, String un2) throws ParseException, FileNotFoundException, IOException, org.apache.lucene.queryparser.classic.ParseException {

       log.debug("Computing the similarity of Tweets....");
        TweetSimilarity ts = new TweetSimilarity();
        List<Tweets> cp = ts.Tweet_Similarity(tweet1, tweet2, un1, un2);
        float score = 0.0f;
        for (int f = 0; f < cp.size(); f++) {
            if (!un1.equals(cp.get(f).getUsername())) {
                score = cp.get(f).getScore();

            }
        }
        log.debug("The score is: " + score);
        return score;
    }

    /**
     * 
     * @param value The value of float file
     * @param places The places of float files
     * @return Rounded number
     */
    public static double round(float value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}