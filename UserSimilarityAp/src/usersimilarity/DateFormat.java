/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usersimilarity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author lebab
 */
public class DateFormat {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DateFormat.class);

    /**
     *
     * @param date The original date
     * @return Stirng - date -
     * @throws ParseException
     */
    public String convert(String date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        Date dateStr = formatter.parse(date);
        date = formatter.format(dateStr);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyy", Locale.ENGLISH);
        date = sdf.format(dateStr);
        log.debug("Parsing date to: " + date + "...");
        return date;
    }
}
