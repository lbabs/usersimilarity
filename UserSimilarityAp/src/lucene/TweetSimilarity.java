/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lucene;

import Constructors.CompareUsers;
import Constructors.Tweets;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

/**
 *
 * @author lebab
 */
public class TweetSimilarity {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TweetSimilarity.class);

    /**
     * A class that contains the similarity between two users
     *
     * @param tweet1 The Tweet of the first user
     * @param tweet2 The tweet of the second user
     * @param un1 The username of the first user
     * @return A list that holds the score (similarity)
     * @throws ParseException
     * @throws FileNotFoundException
     * @throws IOException
     * @throws org.apache.lucene.queryparser.classic.ParseException
     */
    public List<Tweets> Tweet_Similarity(String tweet1, String tweet2, String un1, String un2) throws ParseException, FileNotFoundException, IOException, org.apache.lucene.queryparser.classic.ParseException {

        log.debug("Indexing the data, in order to compute similarity...");




        /**
         * Create the index made from parsing
         */
        String indexPath = "./index";
        Directory index = FSDirectory.open(new File(indexPath));
        String[] indexExists = index.listAll();
        Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
        if (indexExists.length == 0) {
            ParseTweets parseFinal = new ParseTweets();
            List<Tweets> text_tweet = parseFinal.parseDocFile("./data\\Twitter.txt");
            /**
             * Lucene configuration so as to index the text (Tweets)
             */
            IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
            config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

            /**
             * Indexing
             */
            IndexWriter writer = new IndexWriter(index, config);
            AddTweets add = new AddTweets();

            for (Tweets tweet : text_tweet) {
                add.addDoc(writer, tweet);
            }
            writer.close();
        }
        IndexReader reader = DirectoryReader.open(index);
        IndexSearcher searcher = new IndexSearcher(reader);
        List<CompareUsers> compare = new ArrayList<>();
        tweet1 = QueryParser.escape(tweet1);

        /**
         * parse query string with THE SAME ANALYZER USED IN INDEXING
         */
        Query q = new QueryParser(Version.LUCENE_40, "Tweet", analyzer).parse(tweet1);

        List<Tweets> project = new ArrayList<>();
        TopDocs tdocs = searcher.search(q, 450);    //this will return <150 documents                            

        log.debug("Coputing the list that hold the scored tweets...");
        for (ScoreDoc sd : tdocs.scoreDocs) {
            Document doc = reader.document(sd.doc);
            CompareUsers comp = new CompareUsers(doc.get("Username"), doc.get("Tweet"), sd.score);
            compare.add(comp);
        }

        /**
         * Get specific tweets that the user has written. Normalization of
         * Lucene score
         */
        for (int g = 0; g < compare.size(); g++) {
            String tempTweet = tweet1.replace("\\", "");
            String tempTweetSecond = tweet2.replace("\\", "");
            if (tweet2.equals(compare.get(g).getTweets()) && !un1.equals(compare.get(g).getUsername()) && un2.equals(compare.get(g).getUsername())) {

                if (compare.get(g).getTweets().replace(" ", "").equalsIgnoreCase(tempTweet.replace(" ", ""))) {
                    Tweets compSim = new Tweets(0.0f);
                    project.add(compSim);
                } else if (!compare.get(g).getTweets().replace(" ", "").equalsIgnoreCase(tempTweet.replace(" ", ""))) {
                    Tweets comp = new Tweets(compare.get(g).getUsername(), compare.get(g).getTweets(), (1 / (1 + compare.get(g).getScore())));
                    project.add(comp);
                }
            }
            if (!tempTweetSecond.replace(" ", "").contains(compare.toString()) && un1.equals(compare.get(g).getUsername()) && !un2.equals(compare.get(g).getUsername())) {

                Tweets compSim = new Tweets(1.0f);
                project.add(compSim);
            }
        }

        log.debug("Returning the List with the tweet matched and the score");
        return project;
    }
}
