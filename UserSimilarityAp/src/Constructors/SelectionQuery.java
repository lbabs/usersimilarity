package Constructors;

/**
 *
 * @author lebab
 */
public class SelectionQuery {

    String username;
    String tweet;
    String label;
    String date;
    String polarity;

    /**
     *
     * @param un The username of the user
     * @param tw The tweet of the user
     * @param lb The label - Coded date
     * @param dt The date of a single tweet
     * @param pol If a tweet is positive, negative, neutral
     */
    public SelectionQuery(String un, String tw, String lb, String dt, String pol) {
        this.username = un;
        this.tweet = tw;
        this.label = lb;
        this.date = dt;
        this.polarity = pol;
    }

    public String getUsername() {
        return username;
    }

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLabel() {
        return label;
    }

    public String getDate() {
        return date;
    }

    public String getPolarity() {
        return polarity;
    }
}
