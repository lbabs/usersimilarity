/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Constructors;

/**
 *
 * @author lebab
 */
public class Users {

    private String username1;
    private String username2;
    
    public  Users(){
        
    }

    public String getUsername1() {
        return username1;
    }

    public void setUsername1(String username1) {
        this.username1 = username1;
    }

    public String getUsername2() {
        return username2;
    }

    public void setUsername2(String username2) {
        this.username2 = username2;
    }
    
}
